#!/bin/bash

source /etc/environment

consul kv put $APPLICATION_NAME/consul/service/http/name "kong-http"
consul kv put $APPLICATION_NAME/consul/service/http/port "8000"
consul kv put $APPLICATION_NAME/consul/service/http/check/id "HTTP"
consul kv put $APPLICATION_NAME/consul/service/http/check/name "Kong HTTP"
consul kv put $APPLICATION_NAME/consul/service/http/check/tcp "localhost:8000"
consul kv put $APPLICATION_NAME/consul/service/http/check/interval "30s"
consul kv put $APPLICATION_NAME/consul/service/http/check/timeout "1s"

consul kv put $APPLICATION_NAME/consul/service/https/name "kong-https"
consul kv put $APPLICATION_NAME/consul/service/https/port "8443"
consul kv put $APPLICATION_NAME/consul/service/https/check/id "HTTPS"
consul kv put $APPLICATION_NAME/consul/service/https/check/name "Kong HTTPS"
consul kv put $APPLICATION_NAME/consul/service/https/check/tcp "localhost:8443"
consul kv put $APPLICATION_NAME/consul/service/https/check/interval "30s"
consul kv put $APPLICATION_NAME/consul/service/https/check/timeout "1s"

consul kv put $APPLICATION_NAME/consul/service/admin-http/name "kong-admin-http"
consul kv put $APPLICATION_NAME/consul/service/admin-http/port "8001"
consul kv put $APPLICATION_NAME/consul/service/admin-http/check/id "ADMIN-HTTP"
consul kv put $APPLICATION_NAME/consul/service/admin-http/check/name "Kong Admin HTTP"
consul kv put $APPLICATION_NAME/consul/service/admin-http/check/tcp "localhost:8001"
consul kv put $APPLICATION_NAME/consul/service/admin-http/check/interval "30s"
consul kv put $APPLICATION_NAME/consul/service/admin-http/check/timeout "1s"

consul kv put $APPLICATION_NAME/consul/service/admin-https/name "kong-admin-https"
consul kv put $APPLICATION_NAME/consul/service/admin-https/port "8444"
consul kv put $APPLICATION_NAME/consul/service/admin-https/check/id "ADMIN-HTTPS"
consul kv put $APPLICATION_NAME/consul/service/admin-https/check/name "Kong Admin HTTPS"
consul kv put $APPLICATION_NAME/consul/service/admin-https/check/tcp "localhost:8444"
consul kv put $APPLICATION_NAME/consul/service/admin-https/check/interval "30s"
consul kv put $APPLICATION_NAME/consul/service/admin-https/check/timeout "1s"
